const products = [
  {
    id: 1,
    title: 'Product 1',
    price: 9000,
    amount: 90,
    rank: 10,
    url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQLsdNSHNvLy65VwiK8SOmPksffbKVRug6GwKHWQQU0b5s7ngxd'
  },
  {
    id: 2,
    title: 'Product 2',
    price: 8000,
    amount: 80,
    rank: 9,
    url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ7tFDJoQPB48FftFHBS8aLjkhyhFPUhMsX5ce9-hYoSencRYpp'
  },
  {
    id: 3,
    title: 'Product 3',
    price: 7000,
    amount: 70,
    rank: 8,
    url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRSf4XSPzHg1OXgorjk0A3CwgWQJkwKYPs3_Cx8uNvjBe2IPd-y'
  },
  {
    id: 4,
    title: 'Product 4',
    price: 6000,
    amount: 60,
    rank: 7,
    url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ5k734t8Feh0Ps9KGGVlomzSjjZ2byuCCtCbSo9d5TeHjcLwtw'
  },
  {
    id: 5,
    title: 'Product 5',
    price: 5000,
    amount: 50,
    rank: 6,
    url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQMPR83ppXniMCzFVfBvyxeTHsNRSnO_fd8OPM0Ey4yMCVfLowh'
  },
  {
    id: 6,
    title: 'Product 6',
    price: 15000,
    amount: 40,
    rank: 1,
    url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTGIXyBk5sKRvg0_hvz5SyRuzSfYB7KMzN125lFVC7f6N_LS6m4'
  },
  {
    id: 7,
    title: 'Product 7',
    price: 15000,
    amount: 35,
    rank: 2,
    url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT45xeleVw6ewXqHO65fwqpK6SBhNmc26EUZuderiQSv9enOXq2'
  },
  {
    id: 8,
    title: 'Product 8',
    price: 15000,
    amount: 30,
    rank: 3,
    url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRJ6PlhTYIYnEe7IXlbETB6d3l8ve5uHCSqrBIhcl_7VGEZI1pc'
  },
  {
    id: 9,
    title: 'Product 9',
    price: 15000,
    amount: 20,
    rank: 4,
    url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQjQdGxOkU_AfxXQYXycW2VBvQu42nevhD4yA9aijxHPVIvfyqP'
  },
  {
    id: 10,
    title: 'Product 10',
    price: 15000,
    amount: 10,
    rank: 5,
    url: 'https://d2dyh47stel7w4.cloudfront.net/Pictures/2000x2000fit/6/5/7/167657_actimelfruitandvegculturedshots_91752.jpg'
  }
];

export default products;
