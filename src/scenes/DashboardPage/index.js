import { withRouter } from 'react-router-dom';
import Header from 'components/common/Header';
import Sidebar from 'components/common/Sidebar';
import SalesCard from 'components/resources/DashboardPage/SalesCard';
import ProductList from 'components/resources/DashboardPage/ProductList';
import DashboardHeader from 'components/resources/DashboardPage/DashboardHeader';
import ChartPurchase from 'components/resources/DashboardPage/ChartPurchase';
import React, { Component } from 'react';
import products from '../../data/products';

class DashboardPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      topProduct: '',
      anotherProduct:[]
    };
  }

  getBestSelling = (item) => {
    return (
      <ProductList 
        products = {item}
        title ={"BEST SELLING SKU"}
        tipe = {"selling"}
      />
    );
  }

  getTopCompetitor = (Products) => {
    return (
      <ProductList 
        products = {Products}
        title ={"TOP COMPETITOR SKU"}
        tipe = {"competitor"}
      />
    );
  }

  render() {
    return (
      <>
        <div className="dashboard">
          <Header />
          <div className="detail">
            <Sidebar />
            <div className="content">
              <DashboardHeader/>
              <SalesCard />
              <div className="resume">
                <ChartPurchase />
                {this.getBestSelling(products)}
                {this.getTopCompetitor(products)}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default withRouter(DashboardPage);
