import React, { Component } from 'react';
import logo from 'assets/img/advotics.png';
import logout from 'assets/img/logout.png';
import profile from 'assets/img/Profile.svg';
import { Col } from 'react-bootstrap';

class Header extends Component {
  render() {

    return (
      <div className="header">
        <Col className="left" xs={6} sm={8} md={9} lg={9}>
          <img src={logo} className="logo" alt="advotics-logo"/>
          <div className="powered">
            <p className="text">powered by</p>
            <img src={logo} className="logo" alt="advotics-logo"/>
          </div>     
        </Col>          
        <Col className="right" xs={6} sm={4} md={3} lg={3}>
          <div className="user">
            <p className="username">Username</p>
            <p className="company">Company Name</p>
          </div>
          <img src={profile} className="profile" alt="icon-profile"/>
          <img src={logout} className="logout" alt="icon-logout"/>
        </Col>
      </div>
    );
  }
}

export default Header;
