import React, { Component } from 'react';
import more from 'assets/img/more.svg';
import sales from 'assets/img/Sales Turnover.svg';
import downArrow from 'assets/img/DownArrow.svg';

class SalesCard extends Component {
  render() {
    return (
      <div className="sales-card">
        <div className="title">
          <p>Sales Turnover</p>
          <img src={more} className="more-icon" alt="more"/>
        </div>
        <div className="detail">
          <div className="price">
            <h2>Rp 3,600,000</h2>
            <div className="product-sold">
              <img src={downArrow} className="down-arrow" alt="downArrow"/>
              <p className="percentage">13,8%</p>
              <p className="last-period"> last period in products sold</p>
            </div>
          </div>
          <img src={sales} className="sales-icon" alt="sales"/>
        </div>
      </div>
    );
  }
}

export default SalesCard;