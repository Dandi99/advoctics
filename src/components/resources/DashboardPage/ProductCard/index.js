import React, { Component } from 'react';

class ProductCard extends Component {
  render() {
    const { product } = this.props
    return (
      <div className="product-card">
        <img src={product['url']} alt="product" />
        <div className="product-detail">
          <p className="product-name">{product['title']}</p>
          <div className="product-info">
            <p className="product-info-price">{product['price']}</p>
            <p className="product-info-amount">[{product['amount']}]</p>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductCard;