import React, { Component } from 'react';

class TopProductCard extends Component {
  render() {
    const { product } = this.props
    return (
      <div className="top-product-card">
        <img src={product['url']} alt="top-product" />
        <div className="top-product-detail">
          <p className="top-product-name">{product['title']}</p>
          <div className="top-product-info">
            <p className="top-product-info-price">{product['price']}</p>
            <p className="top-product-info-amount">[{product['amount']}]</p>
          </div>
        </div>
      </div>
    );
  }
}

export default TopProductCard;