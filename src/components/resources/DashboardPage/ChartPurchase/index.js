import React, { PureComponent } from 'react';
import {
  BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';
import more from 'assets/img/more.svg';

import { Dropdown } from 'react-bootstrap'

const data = [
  {
    name: 'Jan 12', Nett: 24000, Gross: 2000, APV: 1, UPT:2,
  },
  {
    name: 'Tue', Nett: 23000, Gross: 1100, APV: 1, UPT:2,
  },
  {
    name: 'Wed', Nett: 18000, Gross: 2000, APV: 1, UPT:2,
  },
  {
    name: 'Thu', Nett: 21000, Gross: 2000, APV: 1, UPT:2,
  },
  {
    name: 'Fri', Nett: 17000, Gross: 6000, APV: 1, UPT:2,
  },
  {
    name: 'Sat', Nett: 21000, Gross: 1000, APV: 1, UPT:2,
  },
  {
    name: 'Fun', Nett: 22000, Gross: 1000, APV: 1, UPT:2,
  },
];

class ChartPurchase extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      time: 'last 6 months'
    };
  }

  select = (event) => {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
      time: event.target.innerText
    });
  }

  render() {
    const { time } = this.state
    return (
      <div className="product-list">
        <div className="title">
          <p>AVERAGE PURCHASE VALUE</p>
          <Dropdown className="dropdown">
            <Dropdown.Toggle variant="outline-secondary" className="drop-time">
              { time }
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item onClick={this.select}>last 6 months</Dropdown.Item>
              <Dropdown.Item onClick={this.select}>last month</Dropdown.Item>
              <Dropdown.Item onClick={this.select}>last week</Dropdown.Item>
              <Dropdown.Item onClick={this.select}>today</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
          <img src={more} className="more-icon" alt="more"/>
        </div>
        <div className="chart">
          <BarChart
            width={500}
            height={357}
            data={data}
            margin={{
              top: 20, right: 30, left: 20, bottom: 5,
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Bar dataKey="Nett" stackId="a" fill="#37B04C" maxBarSize='30000' label={{fontSize:'8px'}}/>
            <Bar dataKey="Gross" stackId="a" fill="#289E45" />
            <Bar dataKey="APV" stackId="a" fill="#7AE28C" />
            <Bar dataKey="UPT" stackId="a" fill="#707070" />
          </BarChart>
        </div>
      </div>
    );
  }
}

export default ChartPurchase