import React, {useState} from 'react';
import help from 'assets/img/Help.png';
import { ButtonToolbar, Button, Modal } from 'react-bootstrap'
import TimeCard from '../TimeCard'
import CalenderModal from '../CalenderModal'

const DashboardHeader = () => {

  const [lgShow, setLgShow] = useState(false);

  return (
    <div className="dashboard-header">
      <div className="head">
        <div className="title">
          <h1>Dashboard</h1>
        </div>
        <ButtonToolbar className="toolbar">
          <Button className="filter" variant={"outline"} onClick={() => setLgShow(true)}>
            <TimeCard/>
          </Button>
          <Modal
            size="lg"
            show={lgShow}
            onHide={() => setLgShow(false)}
            aria-labelledby="example-modal-sizes-title-lg"
          >
            <Modal.Header closeButton>
              <Modal.Title id="example-modal-sizes-title-lg">
                <CalenderModal/>
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>...</Modal.Body>
          </Modal>
        </ButtonToolbar>
      </div>
      <div className="subhead">
        <h5>MARKET INSIGHTS</h5>
        <div className="help">
          <img src={help} className="light" alt="help"/>
          <a href="##">Click Here for Help</a>
          <i className="fas fa-chevron-up fa-lg"></i>
        </div>

      </div>
    </div>
  );
};

export default DashboardHeader;
