import React, { Component } from 'react';
import more from 'assets/img/more.svg';
import TopProductCard from '../TopProductCard'
import ProductCard from '../ProductCard'

class ProductList extends Component {

  sortProduct = (products) => {
    const { tipe } = this.props
    var i,j
    if(tipe === "selling"){
      for (i = 0; i < products.length; i++) {
        for (j = i+1; j < products.length; j++) {
          if(products[j]['amount']>products[i]['amount']){
            const temp = products[i]
            products[i] = products[j]
            products[j] = temp
          }
        }
      }
    } else if (tipe === "competitor") {
      for (i = 0; i < products.length; i++) {
        for (j = i+1; j < products.length; j++) {
          if(products[j]['rank']<products[i]['rank']){
            const temp = products[i]
            products[i] = products[j]
            products[j] = temp
          }
        }
      } 
    }
    return products
  }

  renderTopProduct = (products) => {
    return (
      <TopProductCard 
        key = {products[0]['id']}
        product = {products[0]}
      />
    );
  }

  renderAnotherProduct = (products) => {
    var i
    let renderingProduct = []
    for(i = 1; i < 5; i++){
      renderingProduct[i-1]=products[i]
    }
    return (
      renderingProduct.map(product => {
        return (
          <ProductCard
            key = {product['id']}
            product = {product}
          />
        );
      })
    );
  }

  render() {
    const {  title } = this.props
    let { products } = this.props
    products = this.sortProduct(products)

    return (
      <div className="product-list">
        <div className="title">
          <p>{title}</p>
          <img src={more} className="more-icon" alt="more"/>
        </div>
        <div className="products">
          <div className="top-product">
            {this.renderTopProduct(products)}
          </div>
          <div className="another-product"> 
            {this.renderAnotherProduct(products)}
          </div>
        </div>
      </div>
    );
  }
}

export default ProductList;