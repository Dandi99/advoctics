import React from 'react';
import calender from 'assets/img/calendar.png';

const TimeCard = () => {
  return (
    <div className="time-card">
      <img src={calender} className="calender" alt="calender"/>
      <p>period</p>
      <p className="date">11 September 2018 - 14 September 2018</p>
      <i className="arrow fas fa-angle-down fa-2x"></i>
    </div>
  );
};

export default TimeCard;
